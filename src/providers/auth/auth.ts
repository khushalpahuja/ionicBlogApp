import { Injectable } from '@angular/core';
import { Http, Headers } from "@angular/http";
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  apipath = 'http://localhost:8000';
  public user:any;
  public authToken;
  constructor(public http: Http) {
    console.log('Hello AuthProvider Provider');
  }

  signup(user:any) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');      
    return this.http.post( this.apipath + '/register' , user,{headers:headers});
  }

  login(username:string,password:string) {
    console.log("login");
    console.log(this.apipath);
    let headers = new Headers();  
    headers.append('Content-Type','application/json');      
    return this.http.post( this.apipath + '/login' , {username , password},{headers:headers});
}

  logout() {    
    return this.http.get( this.apipath + '/logout');
  }

}
