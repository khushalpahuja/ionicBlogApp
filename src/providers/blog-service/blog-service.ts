// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from "@angular/http";
import 'rxjs/Rx'; 
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';


/*
  Generated class for the BlogServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BlogServiceProvider {
  token:any;
  user:any;
  apipath = 'http://localhost:8000';

  constructor(public http: Http,private storage:Storage) {
    console.log('Hello BlogServiceProvider Provider');
    this.storage.get('token').then((val)=>{
      if(val!=null){
        this.token = val;
      }
    });
    
  }

  setToken(){
    let token;
    this.storage.get('token').then((val)=>{
      console.log("vall",val);
      token=val;
      console.log("token",this.token);
    })
    return token;
  }


  getBlogs(){
    console.log("Sddnj");
    return this.http.get(this.apipath +'/blogs').map(
      (response:Response) => {
        // console.log(response.json());
        return response.json();
      }
    ) 
  }

  getBlog(str:string){
    console.log("xfnhjdhudhgujdhgu");
    // let headers = new Headers(); 
    // headers.append('Authorization',localStorage.getItem('token'));               
      return this.http.get(this.apipath +'/blogs/'+ str ).map(
        (response:Response)=> {
          console.log(response.json());
          // this.blogEdit = response.json().foundBlog;
          return response.json();
        }
      )
    }
  deleteBlog(id:string){
    let headers = new Headers(); 
      // console.log("token",this.token);
      headers.append('Authorization',this.token);
        return this.http.delete(this.apipath +'/blogs/'+ id,{headers:headers});
  }

  createBlog(blog:any){
    console.log("blog",blog);
    let headers = new Headers();    
      headers.append('Authorization',this.token);   
      headers.append('Content-Type','application/json');
      return this.http.post(this.apipath +'/blogs' , blog , {headers:headers});

  }
  addComment(id:string,text:string){
    let headers = new Headers();      
    headers.append('Authorization',this.token); 
    headers.append('Content-Type','application/json');    
      // console.log(text);
      return this.http.post(this.apipath +'/blogs/'+ id +'/comments' ,{text},{headers:headers})
    }

}
