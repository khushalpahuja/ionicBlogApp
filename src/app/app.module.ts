import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { BlogsListPage } from '../pages/blogs-list/blogs-list';
import { BlogServiceProvider } from '../providers/blog-service/blog-service';
import { BlogDetailPage } from '../pages/blog-detail/blog-detail';
import { BlogEditPage } from '../pages/blog-edit/blog-edit';
import { AuthProvider } from '../providers/auth/auth';
import { PopoverComponent } from '../components/popover/popover';
 
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    BlogsListPage,
    BlogDetailPage,
    BlogEditPage,
    PopoverComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    RegisterPage,
    BlogsListPage,
    BlogDetailPage,
    BlogEditPage,
    PopoverComponent    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BlogServiceProvider,
    AuthProvider
  ]
})
export class AppModule {}
