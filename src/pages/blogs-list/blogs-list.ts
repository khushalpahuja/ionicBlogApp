import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { BlogServiceProvider } from '../../providers/blog-service/blog-service';
import { BlogDetailPage } from '../blog-detail/blog-detail';
import { BlogEditPage } from '../blog-edit/blog-edit';
import { PopoverComponent } from '../../components/popover/popover';
/**
 * Generated class for the BlogsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blogs-list',
  templateUrl: 'blogs-list.html',
})
export class BlogsListPage {

  blogs:any[];

  constructor(public navCtrl: NavController, public navParams: NavParams , private blogService:BlogServiceProvider,public popoverCtrl:PopoverController) {
  }
    
  ionViewWillEnter(){
     this.blogService.getBlogs().subscribe(
      (blogs:any[])=>{
        // console.log("blogs:",blogs);
        this.blogs = blogs;
        console.log("after",this.blogs);
      }
    )
  }

  ionViewDidLoad() {
   
    console.log('ionViewDidLoad BlogsListPage');
  }

  onLink(blogClicked:any){
    // console.log("clicked",blogClicked);
    this.navCtrl.push(BlogDetailPage , { blogId: blogClicked });
  }

  addNewBlog(){
    this.navCtrl.push(BlogEditPage);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
  }

}
