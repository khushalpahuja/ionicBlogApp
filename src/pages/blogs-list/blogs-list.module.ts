import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogsListPage } from './blogs-list';

@NgModule({
  declarations: [
    BlogsListPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogsListPage),
  ],
})
export class BlogsListPageModule {}
