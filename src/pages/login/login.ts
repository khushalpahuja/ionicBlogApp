import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { Storage } from '@ionic/storage';
import { AuthProvider } from '../../providers/auth/auth';
import { Response } from '@angular/http';
import { BlogsListPage } from '../blogs-list/blogs-list';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('username') uname;
  @ViewChild('password') password;
  user:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private storage:Storage,private authService:AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signIn(){
    console.log("jndjfbdfbdhbf")
    console.log(this.uname.value);
    console.log(this.password.value);
    this.authService.login(this.uname.value,this.password.value).subscribe(
      (response:Response)=> {
        console.log(response.json().token);    
        this.authService.user = response.json().user;
        this.authService.authToken = response.json().token;
        this.storage.set('token',response.json().token) ;
        this.storage.set('user',JSON.stringify(response.json().user));    
        this.user = this.authService.user;
        this.navCtrl.push(BlogsListPage);    
      }
    )
  }

  

  signUp(){
    this.navCtrl.push(RegisterPage);
  }
  
  // onSubmit(loginForm:NgForm) {
  //   this.authService.login(loginForm.value.username , loginForm.value.password).subscribe(
  //     (response:Response)=> { 

  //       // this.authService.user.username = response.json().user.username;
  //       console.log(response.json());
  //       this.authService.user = response.json().user;
  //       localStorage.setItem('token' ,response.json().token);
  //       localStorage.setItem('user' , JSON.stringify(response.json().user));
  //       this.authService.setUser();
  //       this.user = this.authService.user;
  //       this.authService.chnageUsername.next(this.authService.user); 
  //       console.log(this.authService.user);
  //       this.router.navigate(['/blogs']);
  //     }
  //   );
  // }
  

}
