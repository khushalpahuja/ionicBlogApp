import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlogEditPage } from './blog-edit';

@NgModule({
  declarations: [
    BlogEditPage,
  ],
  imports: [
    IonicPageModule.forChild(BlogEditPage),
  ],
})
export class BlogEditPageModule {}
