import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BlogServiceProvider } from '../../providers/blog-service/blog-service';
import { Response } from '@angular/http';
import { BlogsListPage } from '../blogs-list/blogs-list';

/**
 * Generated class for the BlogEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-edit',
  templateUrl: 'blog-edit.html',
})
export class BlogEditPage {
  blog={};
  constructor(public navCtrl: NavController, public navParams: NavParams,private blogService:BlogServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogEditPage');
  }

  submitForm() {
     this.blogService.createBlog(this.blog).subscribe(
      (response:Response) => {
        // this.blogsUpdated.next(response.json());
        // this.blogs.push(response.json());
        console.log(response.json());
        this.navCtrl.push(BlogsListPage);
    
      }
    );
  }

}
