import { Component, ViewChild } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { BlogsListPage } from '../blogs-list/blogs-list';
import { PopoverComponent } from '../../components/popover/popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild('username') uname;
  @ViewChild('password') password;

  constructor(public navCtrl: NavController,public popoverCtrl:PopoverController) {

  }

  signIn(){
    this.navCtrl.push(LoginPage);
  }
  
  signUp(){
    this.navCtrl.push(RegisterPage);
  }

  showBlogList(){
    this.navCtrl.push(BlogsListPage);
  }



}
