import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { BlogServiceProvider } from '../../providers/blog-service/blog-service';
import { LoadingController } from 'ionic-angular';
import { PopoverComponent } from '../../components/popover/popover';
import { AlertController } from 'ionic-angular';
import { BlogsListPage } from '../blogs-list/blogs-list';
import { ModalController } from 'ionic-angular';
// import {  }
/**
 * Generated class for the BlogDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blog-detail',
  templateUrl: 'blog-detail.html',
})
export class BlogDetailPage {
  blogId:string;
  blog:any;
  comments:any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams , 
              private blogService:BlogServiceProvider ,
              private loadingCtrl:LoadingController,
              public popoverCtrl:PopoverController,
              public alertCtrl: AlertController,
              public modalCtrl : ModalController
            )
               {

                  this.blogId = navParams.get('blogId');
                  console.log(this.blogId);
                }

  ionViewWillEnter() {
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present();    
    console.log("dgnujgndj");
    this.blogService.getBlog(this.blogId).subscribe(
      (data:any)=>{
        console.log("dfjfhdjf");
        this.blog = data.foundBlog;
        // console.log(this.blog.author.username);
        this.comments = data.comment;
        loader.dismiss();
        // this.likecount = this.blog.likecount;
        // if(this.blog.author.id == this.authService.user._id || this.authService.user.type==='admin'){
        //   this.isBlogOwner = true;
        // } else {
        //   this.isBlogOwner = false;
        // }
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlogDetailPage');
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverComponent);
    popover.present({
      ev: myEvent
    });
  }

  onDelete(){
    this.showConfirm();
  }

  addComment(){
    var modalPage = this.modalCtrl.create('AddCommentPage',{ userId : this.blogId});
    modalPage.present();
  }


  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Delete Blog?',
      message: 'Are you sure you want to delete this blog',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.blogDelete();    
          }
        }
      ]
    });
    confirm.present();
  }

  blogDelete(){
    this.blogService.deleteBlog(this.blogId).subscribe(
      (res:any)=> {
        console.log(res.json());
        this.navCtrl.push(BlogsListPage);
      }
    )

  }

  

}
