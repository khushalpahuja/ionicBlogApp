import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BlogServiceProvider } from '../../providers/blog-service/blog-service';
import { Response } from '@angular/http';
// import { BlogDetailPage } from '../blog-detail/blog-detail';
import { BlogsListPage } from '../blogs-list/blogs-list';

/**
 * Generated class for the AddCommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-comment',
  templateUrl: 'add-comment.html',
})
export class AddCommentPage {
  blogId:any;
  text:string='';
  constructor(public navCtrl: NavController, public navParams: NavParams,private blogService:BlogServiceProvider) {
    this.blogId = this.navParams.get('userId');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCommentPage');
  }

  addComment(){
    this.blogService.addComment(this.blogId,this.text).subscribe(
      (res:Response)=> {
        console.log(res.json());
        this.navCtrl.push(BlogsListPage);
      }
    )
  }

}
