import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { AuthProvider } from '../../providers/auth/auth';
import { Response } from '@angular/http';
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
user={};
  constructor(public navCtrl: NavController, public navParams: NavParams,private authProvider:AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  signUp(){
    console.log(this.user);
    this.navCtrl.pop();
    this.authProvider.signup(this.user).subscribe(
      (res:Response)=>{
        console.log("response");
      }
    )
  }

  signIn(){
    this.navCtrl.push(LoginPage)
  }

}
