import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController,App, ViewController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {

  text: string;

  constructor(public storage:Storage,private navCtrl:NavController,public appctrl:App,public viewCtrl:ViewController) {
    console.log('Hello PopoverComponent Component');
    this.text = 'Hello World';
  }

  logout(){
    console.log("logout");
    this.viewCtrl.dismiss()
    this.appctrl.getRootNav().push(HomePage);
    this.storage.clear();
  }

}
